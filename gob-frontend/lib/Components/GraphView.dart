import 'dart:core';
import 'dart:math';

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:frontend/Models/Usage.dart';
import 'package:intl/intl.dart';

class GraphView extends StatefulWidget {
  final bool animate;
  final List<charts.Series<Usage, DateTime>> series;
  final String units;

  final charts.RangeAnnotation highlightRange;

  final charts.LineRendererConfig<DateTime> rendererConfig =
      charts.LineRendererConfig(
    // ID used to link series to this renderer.
    customRendererId: 'area',
    includeArea: true,
  );

  final charts.DateTimeAxisSpec axisSpec = charts.DateTimeAxisSpec(
//      tickProviderSpec: charts.DayTickProviderSpec(increments: [5]),
      tickProviderSpec: charts.AutoDateTimeTickProviderSpec(),
//      tickProviderSpec: charts.StaticDateTimeTickProviderSpec([charts.TickSpec(DateTime(2019, 1, 1, 8))]),
      showAxisLine: true,
      renderSpec: charts.GridlineRendererSpec(
        labelStyle: charts.TextStyleSpec(color: charts.Color.white),
        lineStyle: charts.LineStyleSpec(
          color: charts.Color.fromHex(code: "878787FF"),
        ),
        axisLineStyle: charts.LineStyleSpec(color: charts.Color.white),
      ));

  final charts.NumericAxisSpec primaryAxisSpec = charts.NumericAxisSpec(
    tickProviderSpec: charts.BasicNumericTickProviderSpec(
        dataIsInWholeNumbers: false, desiredMinTickCount: 8),
    renderSpec: charts.SmallTickRendererSpec(
        labelStyle: charts.TextStyleSpec(color: charts.Color.white)),
  );

  GraphView(this.series, this.units, {this.animate, this.highlightRange});

  factory GraphView.createGraphView(
      List<Usage> currentUsageList, String units, DateFormat dateFormat,
      {List<Usage> compareUsageList, animate, bool highlightPeaks=false}) {

    var highlight;

    if (highlightPeaks) {
      DateTime graphTime = currentUsageList.first.startTime;

      highlight = charts.RangeAnnotation([
        charts.RangeAnnotationSegment(
            DateTime(graphTime.year, graphTime.month, graphTime.day, 16), DateTime(graphTime.year, graphTime.month, graphTime.day, 19), charts.RangeAnnotationAxisType.domain,
            startLabel: 'Peak Times', color: charts.ColorUtil.fromDartColor(Color.fromARGB(150, 156, 16, 47)), labelStyleSpec: charts.TextStyleSpec(color: charts.ColorUtil.fromDartColor(Colors.white)))
      ]);
    } else {
      highlight = charts.RangeAnnotation([]);
    }
    return GraphView(
        _createSeries(currentUsageList, compareUsageList, dateFormat), units,
        animate: animate, highlightRange: highlight);
  }

  static List<charts.Series<Usage, DateTime>> _createSeries(
      List<Usage> currentUsageList,
      List<Usage> compareUsageList,
      DateFormat dateFormat) {
    currentUsageList.sort((a, b) => a.startTime.compareTo(b.startTime));

    // Get the string of the current month
    var currentMonth = dateFormat.format(currentUsageList[0].startTime);
    var series = [
      charts.Series<Usage, DateTime>(
        id: currentMonth,
        colorFn: (_, __) => charts.ColorUtil.fromDartColor(Color(0xFF9C27B0)),
        domainFn: (Usage sales, _) => sales.startTime,
        measureFn: (Usage sales, _) => sales.consumption,
        data: currentUsageList,
      )..setAttribute(charts.rendererIdKey, 'area')
    ];
    if (compareUsageList != null) {
      List<Usage> fixedComparison = [];
      compareUsageList.sort((a, b) => a.startTime.compareTo(b.startTime));
      var compareMonth = dateFormat.format(compareUsageList[0].startTime);
      for (var e
          in IterableZip(<Iterable>[compareUsageList, currentUsageList])) {
        fixedComparison.add(
            Usage(startTime: e[1].startTime, consumption: e[0].consumption));
      }
      series.add(charts.Series<Usage, DateTime>(
        id: compareMonth,
        colorFn: (_, __) => charts.ColorUtil.fromDartColor(Color(0xFFC3424C)),
        domainFn: (Usage sales, _) => sales.startTime,
        measureFn: (Usage sales, _) => sales.consumption,
        data: fixedComparison,
      )..setAttribute(charts.rendererIdKey, 'area'));
    }

    return series;
  }

  factory GraphView.withSampleData() {
    return GraphView.createGraphView(
        _createSampleData(), "Test", DateFormat.d());
  }

  @override
  GraphViewState createState() => GraphViewState();

  // Create one series with sample random coded data.
  static List<Usage> _createSampleData() {
    List<Usage> fakeMonthData = [];
    var rng = Random();
    for (var i = 1; i <= 31; i++)
      fakeMonthData.add(Usage(
          startTime: DateTime(2018, 11, i),
          consumption: (50 + rng.nextInt(15)) / 10));

    return fakeMonthData;
  }
}

class GraphViewState extends State<GraphView> {
  DateTime _time;
  Map<String, num> _measures;
  List<charts.Series<Usage, DateTime>> series;

  List<String> endings = List.generate(31, (int index) {
    if ([0, 20, 30].contains(index)) return "st";
    if ([1, 21].contains(index)) return "nd";
    if ([2, 22].contains(index))
      return "rd";
    else
      return "th";
  });

  _onSelectionChanged(charts.SelectionModel model) {
    final selectedDatum = model.selectedDatum;

    DateTime time;
    final measures = <String, num>{};

    if (selectedDatum.isNotEmpty) {
      time = selectedDatum.first.datum.startTime;
      selectedDatum.forEach((charts.SeriesDatum datumPair) {
        measures[datumPair.series.displayName] = datumPair.datum.consumption;
      });
    }

    setState(() {
      _time = time;
      _measures = measures;
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final children = <Widget>[
      Expanded(
        child: charts.TimeSeriesChart(
          widget.series,
          animate: widget.animate,
          customSeriesRenderers: [widget.rendererConfig],
          primaryMeasureAxis: widget.primaryAxisSpec,
          domainAxis: widget.axisSpec,
          behaviors: [
            charts.ChartTitle(widget.units,
                behaviorPosition: charts.BehaviorPosition.start,
                titleStyleSpec: charts.TextStyleSpec(
                    fontSize: 14, color: charts.Color.white),
                titleOutsideJustification:
                    charts.OutsideJustification.middleDrawArea),
            charts.SeriesLegend(
                entryTextStyle:
                    charts.TextStyleSpec(color: charts.Color.white)),
//            charts.SelectNearest(
//                eventTrigger: charts.SelectionTrigger.tapAndDrag),
            charts.PanAndZoomBehavior(),
            widget.highlightRange,
          ],
          selectionModels: [
            charts.SelectionModelConfig(
                type: charts.SelectionModelType.info,
                changedListener: _onSelectionChanged)
          ],
        ),
      )
    ];

    if (_time != null) {
      children.add(Padding(
          padding: EdgeInsets.only(top: 5),
          child: Text(
              "${_time.day}${endings[_time.day - 1]} ${DateFormat.MMMM().format(_time)} ${DateFormat.Hm().format(_time)}",
              style: Theme.of(context).textTheme.subhead)));
    }

    _measures?.forEach((String series, num value) {
      children.add(Text(
        '$series: ${value.toStringAsFixed(2)} ${widget.units}',
        style: Theme.of(context).textTheme.subhead,
      ));
    });

    return Column(children: children);
  }
}
