package gamifybills.gobbackend;

import static org.assertj.core.api.Assertions.assertThat;

import gamifybills.gobbackend.model.EnergyUsage;
import gamifybills.gobbackend.model.User;
import gamifybills.gobbackend.repository.EnergyUsageRepository;
import gamifybills.gobbackend.repository.RoleRepository;
import gamifybills.gobbackend.repository.UserRepository;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@DataJpaTest
public class EnergyUsageRepositoryIntegrationTest {
  @Autowired
  private RoleRepository roleRepository;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private EnergyUsageRepository energyUsageRepository;

  @Test
  public void whenFindByEnergyUsageID_Username_thenReturnListEnergyUsage() {
    User test = new User("test", "pass");
    userRepository.save(test);
    userRepository.flush();

    EnergyUsage testUsage = new EnergyUsage("test", Timestamp.from(Instant.now()), 1.0);
    energyUsageRepository.save(testUsage);

    List<EnergyUsage> found = energyUsageRepository.findByEnergyUsageID_Username(test.getUsername());

    assertThat(found.get(0).getEnergyUsageID().getStartTime())
        .isEqualTo(testUsage.getEnergyUsageID().getStartTime());
  }

  @Test
  public void whenFindInterval_thenReturnListEnergyUsage() {
    User test = new User("test", "pass");
    userRepository.save(test);
    userRepository.flush();

    EnergyUsage janUsage = new EnergyUsage("test", Timestamp.valueOf("2019-01-15 00:00:00"), 1.0);
    EnergyUsage febUsage = new EnergyUsage("test", Timestamp.valueOf("2019-02-15 00:00:00"), 1.0);
    EnergyUsage marUsage = new EnergyUsage("test", Timestamp.valueOf("2019-03-15 00:00:00"), 1.0);
    energyUsageRepository.save(janUsage);
    energyUsageRepository.save(febUsage);
    energyUsageRepository.save(marUsage);

    List<EnergyUsage> found = energyUsageRepository.findAllByEnergyUsageID_UsernameAndEnergyUsageID_StartTimeGreaterThanEqualAndEnergyUsageID_StartTimeLessThan("test", Timestamp.valueOf("2019-01-17 00:00:00"), Timestamp.valueOf("2019-04-15 00:00:00"));

    assertThat(found.get(0).getEnergyUsageID().getStartTime())
        .isEqualTo(febUsage.getEnergyUsageID().getStartTime());

    assertThat(found.get(1).getEnergyUsageID().getStartTime())
        .isEqualTo(marUsage.getEnergyUsageID().getStartTime());
  }
}

