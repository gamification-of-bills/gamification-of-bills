import 'dart:convert';

class LoginResponse {
  final String accessToken;
  final String tokenType;
  final int iat;
  final int exp;

  LoginResponse({this.accessToken, this.tokenType, this.iat, this.exp});

  factory LoginResponse.fromJson(Map<String, dynamic> jsonData) {
    String accessToken = jsonData['accessToken'];
    // Decode claims section of b64 access token for expiry info
    var claims = accessToken.split(".")[1];
    if (claims.length % 4 != 0) claims += "=" * (4 - (claims.length % 4));
    var decoded = jsonDecode(utf8.decode(base64Decode(claims)));
    return LoginResponse(
        accessToken: accessToken,
        tokenType: jsonData['tokenType'],
        iat: decoded['iat'],
        exp: decoded['exp']);
  }
}
