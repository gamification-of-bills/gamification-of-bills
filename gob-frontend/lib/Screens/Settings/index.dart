import 'package:flutter/material.dart';
import 'package:frontend/Components/UniversalScaffold.dart';
import 'package:preferences/preferences.dart';

class SettingsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SettingsScreenState();
}

class SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return UniversalScaffold(
      appBarParams: AppBarParams(title: Text('Preferences')),
      route: '/settings',
      body: PreferencePage([
        PreferenceTitle('General'),
        DropdownPreference(
          'Default Unit',
          'default_unit',
          defaultVal: '£/H',
          values: ['kWh', '£/H', 'Nissan Leaf', '2 Litre Kettles'],
        ),

      ]),
    );
  }
}
