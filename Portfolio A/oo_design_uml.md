# OO Design & UML

## Architecture

![](images/architecture.png)

The application will consist of several parts, An app for the end user, a database for storing usage information and an API for communicating with the database and the app. Throughout development data will be provided as a CSV with the following format:

__id__ (unique account number, `integer`), __start_time__ (UTC, format _2017-09-03 13:00:00 UTC_ ), __consumption__ (KWh, `float`: _0.072_)

This will be added to the database via simple a script.

The usage database will be a MySQL DB (using MariaDB) with the following layout:

```mssql
energy_usage (id INTEGER PRIMARY KEY, start_time TIMESTAMP, consumption FLOAT)
```

The database will be only accessible by the API, therefore by ensuring sanitised inputs from the app, we can keep all data secure.

The API will be RESTful and implemented using Spring (Java). Whilst the functionality of the API will be minimal at first, it can be expanded to provide more powerful functionality. By processing data on a server, we can both ensure data security and allow more expensive computations.

A further table is added to the server to allow for user authorisation, simply storing a numeric id, a username and a hashed password.

## Static UML

We deemed static UML unnecessary for the API as it does not possess any relations or inheritance. The API needs minimal functionality for the MVP:
- `/auth`: An endpoint to authorise a user with the api
- `/interval`(`startPoint`, `endPoint`): An endpoint that allows the app to retrieve data between two timestamps, which will be in UTC __ISO 8601__ format (e.g _2018-11-15T17:46:22Z_)

The app will be created using Flutter (Dart). This framework allows us to create native cross-platform applications in an object orientated way. The structure of our app is laid out below:

![](images/flutter.png)

By using these various classes we can helpfully reuse elements of code and easily model an application state as throughout the program. The main UniversalScaffold is used to allow the key elements of every page to be automatically inserted (such as an app bar and drawer). A single REST data source is created that manages making requests and formatting them for external API. It also allows for a custom caching system to be managed within the app.

The database contains no relations, and will follow the schema given above. A second table will potentially be used to maintain a reference of user authorisation tokens tokens.

## Dynamic UML

Below is a diagram of an example usage of that app and how data is transferred between various parts of the architecture

![](images/authenticated_flow.png)

A second dynamic diagram was also created to show how the authentication process takes place. We modelled this because of the importance of the system and to clarify it for future reference.

![](images/unauthenticated_flow.png)

## Reflection

Modelling the system in these various ways has provided an invaluable reference for future development, as well as provoking questions about how the overall system will function, e.g. The flow of authentication and the best system to use in this case.
