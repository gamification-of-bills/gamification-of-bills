package gamifybills.gobbackend.model;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "energy_usages")
public class EnergyUsage {

	@EmbeddedId
	private EnergyUsageID energyUsageID;

	@Column(name = "consumption")
	private Double consumption;

	public EnergyUsage() {
	}

	public EnergyUsage(String username, Timestamp timestamp, Double consumption) {
		this.energyUsageID = new EnergyUsageID(username, timestamp);
		this.consumption = consumption;
	}

	public EnergyUsageID getEnergyUsageID() {
		return energyUsageID;
	}

	public Double getConsumption() {
		return consumption;
	}
}
