# Gamification of Energy Bills

## Timeline

There are several key deadlines for this project, shown here:

![timeline](images/Timeline.png)

## Overview

See [Porfolio A Overview](Portfolio A/overview.md)

## Constraints

Should be developed as a native app (iOS and / or Android) or as a web app that is supported by the majority of mobile browsers (Safari, Chrome and Samsung Internet collectively make up ~95% of the UK mobile browser market share ).

This should be developed as an app, data from the smart meter will be received via xx. This data can be provided as a separate file as an interim stage to develop the product. Integration to more “live” data sources can be a secondary step. We also need to be mindful that customers’ smart meter data should be kept private.

## Research

There are many existing apps for monitoring the usage of energy within a home, here a some that we have researched:

- [EnergyCloud](https://www.bluelineinnovations.com/features): 
  - Provides real time feedback
  - The ability to view trends of usage giving feedback on whether cost saving measures are working
  - Comparing results of different months
  - Analyzing habits
  - Access information from anywhere
- [Nest app](https://nest.com/uk/app/):
  - Access information from anywhere
  - Insights into usage
  - Share information with others
- [Neurio](https://www.neur.io/):
  - Similar features to EnergyCloud
  - Works with solar panels and renewables
- [Wattsly](https://energyappsontario.devpost.com/submissions/20053-wattsly):
  - Graphs of usage that have changeable units
    - kWh, cost, range of an electric car, etc.
  - Ability to tag certain times with what appliances you were using
- [TheGenGame](https://www.thegengame.com/):
  - Notificications at peak times to try and reduce
  - Earn points based on how much you can save
  - Leagues for competing users

The best examples for inspiration are "Wattsly" and "TheGenGame". These give good examples of processing information and making it easy to understand and engage with. The notification system that TheGenGame employs is a great idea, and being able to prompt different sections of the a userbase to reduce usage would help even out the load on the grid - This would require some fairly complex algorithms however.

The other common feature that can be seen across examples is the ability to see your usage wherever you are. This is a desirable feature as it allows users to monitor their usage at all times, spotting if they have something wasting energy, seeing times it would have been good to use more energy, etc. 

## Requirements

See [Portfolio A Requirements](Portfolio A/requirements.md)

## OO Design & UML

See [Portfolio A OO Design & UML](Portfolio A/oo_design_uml.md)

## Testing

See [Portfolio A Testing](Portfolio A/testing.md)

# How to install

There are two repositories, one for the backend and frontend, found at the following links:

- https://gitlab.com/gamification-of-bills/gob-backend
- https://gitlab.com/gamification-of-bills/gob-frontend

