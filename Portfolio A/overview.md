
# Overview

The goal of this project is to create an app that will help increase user engagement with their energy bills, by creating an app that "gamifies" the information and allows them to easily see their energy usage, incentivising them to reduce it or spread it out and avoid peak hours. The app will contain many features a few such examples are: 
-   comparing past usage (e.g. current month/week compared to previous), allowing them to compare their spending with others in their area.
-   providing a reward points system for good usage.
-   another possible feature would be to integrate a simple game into the app that offers bonuses for high scores.

However, we will first focus on a subset of these features and improve upon this if possible.

## Client & Domain model

Our clients for this project are Baringa Partners, OVO Energy and Bristol Energy. Baringa acts as consultants for many businesses, including OVO and Bristol Energy, and are looking to reduce the strain and cost of peak usage for their clients. OVO and Bristol energy would gain from not having to buy energy from the producers during peak hours, so an app based solution to help users with this would both be good for the environment and the companies.

Throughout this project we will be working with several consultants from Baringa, along with some contacts within the sales and technical team at OVO and Bristol energy.

To aid the design of our system, a domain model has been created to show the existing systems and how they interact:

![](images/domain_model.png)

## MVP	

For the minimum viable product, we plan to implement a simple app that:
- will allow a user to see their current usage and compare it to previous usage (e.g. the last month, week etc.). 
- will be able to give the user a real sense of how much they actually consume by giving real-world examples
- will be based on the data provided by OVO and will be half-hourly consumption data received from homes with smart meters(or a local database mock of that if the data is not available until MVP release). 
- for each user will have an ID (linking their energy bills and usage data) which will allow them to login and see their data
- should include a mock-up of the tip based system for the consumers
- will have a front end built using Flutter
- Will be backed up by an API using Java Spring
- should be testable so that we may receive feedback until stage 2 of the project in term 2

## Final product 

The final product is subject to change based on the feedback received on the MVP and Beta releases.
Up until now, those are some of the planned features:

- Give users an overall dashboard to show their progress saving energy 	

- Allow easy conversions of units on graphs (for more relateable measures)	

- Highlight peak times to avoid using energy 	

- Provide user with goals 	

- Tips for reducing usage and real-life comparisons (ex. you could power X washing machines for this time with X amount of power)

## Future ideas

- A built-in simple game that could offer bonuses (green products) on services based on high scores and rankings in the local area

- Integration of "live" data sources




