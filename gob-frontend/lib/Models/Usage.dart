class Usage {
  final DateTime startTime;
  final double consumption;

  Usage({this.startTime, this.consumption});

  factory Usage.fromJson(Map<String, dynamic> json) {
    return Usage(
      startTime: DateTime.parse(json['startDate']),
      consumption: json['consumption'],
    );
  }
}
