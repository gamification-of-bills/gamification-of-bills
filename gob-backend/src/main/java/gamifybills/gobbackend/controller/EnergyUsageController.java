package gamifybills.gobbackend.controller;

import gamifybills.gobbackend.model.EnergyUsage;
import gamifybills.gobbackend.payload.IntervalResponse;
import gamifybills.gobbackend.payload.MonthStatsResponse;
import gamifybills.gobbackend.repository.EnergyUsageRepository;
import gamifybills.gobbackend.security.CurrentUser;
import gamifybills.gobbackend.security.UserPrincipal;
import java.sql.Timestamp;
import java.time.temporal.TemporalAdjusters;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping(value = "/api")
public
class EnergyUsageController {

  @Autowired
  EnergyUsageRepository energyUsageRepository;

  /**
   * Mapping for /interval api method
   *
   * @param currentUser Logged in user to retrieve data for
   * @param startDate Start date of requested data. Defaults to epoch time
   * @param endDate End date of requested data. Defaults to the end of epoch time
   * @param resolution Resolution of data for output (0: Half hourly, 1: Hourly, 2: Daily)
   * @return IntervalResponse object that manages the processing and formatting of data
   */
  @RequestMapping("/interval")
  public IntervalResponse interval(@CurrentUser UserPrincipal currentUser,
                                   @RequestParam(value = "startDate", defaultValue = "1970-01-01T00:00:00Z") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
                                   @RequestParam(value = "endDate", defaultValue = "2038-01-19T00:00:00Z") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate,
                                   @RequestParam(value = "resolution") int resolution) {
//    List<EnergyUsage> energyUsages = energyUsageRepository.findByEnergyUsageID_Username(currentUser.getUsername());
    System.out.println(currentUser);

    List<EnergyUsage> energyUsages = energyUsageRepository.findAllByEnergyUsageID_UsernameAndEnergyUsageID_StartTimeGreaterThanEqualAndEnergyUsageID_StartTimeLessThan(currentUser.getUsername(), Timestamp.valueOf(startDate), Timestamp.valueOf(endDate));
    return new IntervalResponse(startDate, endDate, energyUsages, resolution);
  }

  @RequestMapping("/stats")
  public MonthStatsResponse stats(@RequestParam(value = "startDate", defaultValue = "2019-01-01T00:00:00Z") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime month) {

    List<EnergyUsage> energyUsages = energyUsageRepository.findAllWithMonth(Timestamp.valueOf(month.with(
        TemporalAdjusters.firstDayOfMonth())));

    return new MonthStatsResponse(month, energyUsages);
  }
}