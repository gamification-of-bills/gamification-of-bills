class StatsResponse {
  final DateTime startDate;
  final double average;
  final double min;
  final double max;

  StatsResponse({this.startDate, this.average, this.min, this.max});

  factory StatsResponse.fromJson(Map<String, dynamic> json) {
    return StatsResponse(
      startDate: DateTime.parse(json['startDate']),
      average: json['average'],
      min: json['min'],
      max: json['max'],
    );
  }
}
