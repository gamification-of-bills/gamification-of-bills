# Development Testing

## Component Testing

Referring to the architecture diagram shown [previously](oo_design_uml.md):

- The app will be tested with Flutter's unit and widget/component tests in order to maintain stability and functionality.
- The API will be tested via unit tests to ensure it communicates with the front-end and the database without errors.
- The database is subject to security tests to ensure the integrity of the data whilst preventing unauthorized access to it.
- The program that will import CSV data to the database will be Unit tested to prevent any mishaps in handling the data.
- The entirety of the project will undergo integration tests accounting for its best functionality with the least amount of bugs possible.
- Using both emulators and real phones, we test potential use cases of the app to ensure intended results.

## Testing Frameworks

During development testing, we will make use of

- Flutter tests
- Flutter driver tests
- JUnit

Flutter provides Unit, Widget/Component, and Integration tests, while JUnit will serve as a more familiar unit testing tool for the backend. 

Flutter unit testing allows for the simple creation of individual Widgets and verifying their functionality. This allows for very rapid testing of the application, however, it doesn't test how components work together, and gives a very limited overview of the overall app.

Flutter driver tests allow for a much more thorough test of the whole app by using set actions and inputs. We can use this to test systems where the individual components might not work correctly together, for example automatically checking logins work. Once this system is in place, it is easy to thoroughly test the functionality of a program by simply running this.

## Challenges

For most of us, this is the first big project that we are developing ourselves, and Flutter is a new tool for all of us. Developing testing functions with new frameworks is a challenge we are all looking forward to overcoming.

The most important challenge would be testing on iOS devices. Since we don't own Mac OS, we are unable to generate a .ipa file from the flutter code. Workarounds that sideload apps can't work without the .ipa file, so, unfortunately, we were unable to overcome this. We have to rely on flutter building the code correctly and assume that if it works for android, it should work on iOS too. If we were to have a Mac computer, we would use Apple's Xcode IDE to generate the .ipa file and quickly test either with the built-in emulator or our iPhones. 

To ensure the normal operation of our program, we have to test what would be realistic data that might come from our providers. Using different sets of abstract data, such as normal data, data at the limits or  even abnormal data, it is possible to have a system prepared for potential extreme or unexpected values. As such, we generate random data and build our program to work with it. By ensuring the complete coverage of potential data, not only would the switch to real data be easy, but we also ensure that we won't run into problems when we are given a dataset for another time period.

A lot of the testing done is making sure the UI looks good and adjusting it accordingly. Our program works on both Android and iOS, which means that it would be a challenge to optimize it for every single phone's display and make the UI look good. Using different mobile screen sizes and evaluating is a key priority for our UI testing, so to overcome this challenge we use various emulators and real phones, ensuring it looks as expected on resolutions of popular devices. 

Using flutter's Hot Reload feature, changes made to the code can be updated live to the process ran by the phone, for immediate cause-effect analysis of such changes.

## CI/CD

We also use CI/CD to our advantage with this project. Using a configuration in the root of both repositories, we can define actions for Gitlab to take with every commit. For the app, this involves running all tests, collecting coverage, building the app and providing the built APK available for download. This allows for automated releases, as well as checking that feature branches do not contain any broken code. A similar system is in place for the server that runs all tests and builds a war file for deployment.
