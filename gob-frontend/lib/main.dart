import 'package:frontend/Routes.dart';
import 'package:preferences/preferences.dart';

void main() async {
  await PrefService.init(prefix: 'pref_');
  Routes();
}
