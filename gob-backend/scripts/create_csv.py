import csv
import datetime
import random
import string
import bcrypt

################################################################################
# Creates 3 files:
#   logins.csv, a list of username password combinations
#   login_db.csv, a list of username and password hash
#       combos for storage in db
#   energy_usage.csv, a list of datapoints for use in app
################################################################################

NUM_IDS = 10
NUM_DATAPOINTS = 18000  # 3000 About 2 months

login_file = open('logins.csv', mode='w')
login_writer = csv.writer(login_file)

login_db_file = open('login_db.csv', mode='w')
login_db_writer = csv.writer(login_db_file)

csv_file = open('energy_usage.csv', mode='w')
csv_writer = csv.writer(csv_file)

for ident in range(NUM_IDS):
    pw = ''.join(random.choice(string.ascii_uppercase + string.digits)
                 for _ in range(10))
    salt = bcrypt.gensalt(rounds=10, prefix=b"2a")
    hashed = bcrypt.hashpw(pw.encode(), salt)

    login_writer.writerow([ident, pw])
    login_db_writer.writerow([ident, hashed.decode()])

    for i in range(NUM_DATAPOINTS):
        t = datetime.datetime(2018, 6, 1) + datetime.timedelta(minutes=i*30)
        csv_writer.writerow([ident, t.isoformat(), random.random()*2])
csv_file.close()
login_file.close()
login_db_file.close()
