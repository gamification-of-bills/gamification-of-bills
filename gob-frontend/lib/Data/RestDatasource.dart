import 'dart:convert';
import 'dart:io';

import 'package:frontend/Cache/Cache.dart';
import 'package:frontend/Cache/MemCache.dart';
import 'package:frontend/Models/IntervalResponse.dart';
import 'package:frontend/Models/LoginResponse.dart';
import 'package:frontend/Models/StatsResponse.dart';
import 'package:frontend/app_config.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class RestDatasource {
  static final RestDatasource _restDatasource = RestDatasource._internal();

  factory RestDatasource() {
    return _restDatasource;
  }

  RestDatasource._internal();

  static final baseUrl = defaultConfig.apiUrl;
  static final loginUrl = baseUrl + "/auth/signin";

  final completedRequests = Set<int>();

  final Cache<IntervalResponse> intervalCache = MemCache<IntervalResponse>();

  Future<LoginResponse> login(
      http.Client client, String username, String password) async {
    Map<String, String> headers = {'Content-type': 'application/json'};
    final response = await client.post(loginUrl,
        body: json.encode({"username": username, "password": password}),
        headers: headers);
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON
      return LoginResponse.fromJson(json.decode(response.body));
    } else {
//      print("${response.statusCode}, ${response.body}");
      // If that response was not OK, throw an error.
      throw Exception("Error logging in");
    }
  }

  Future<IntervalResponse> fetchIntervalData(http.Client client, int resolution,
      [DateTime startDate, DateTime endDate]) async {
    if (startDate == null || endDate == null) return null;

    var body = {
      "resolution": resolution.toString(),
      "startDate": startDate.toIso8601String(),
      "endDate": endDate.toIso8601String()
    };

    var bodyHash = body.toString().hashCode;

    if (completedRequests.contains(bodyHash)) {
      return intervalCache.get(bodyHash);
    } else {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      final response = await client.post('$baseUrl/api/interval',
          body: body,
          headers: {
            HttpHeaders.authorizationHeader:
                "Bearer ${prefs.getString("accessToken")}"
          });

      if (response.statusCode == 200) {
        // If server returns an OK response, parse the JSON
        IntervalResponse result =
            IntervalResponse.fromJson(json.decode(response.body));
        completedRequests.add(bodyHash);
        intervalCache.put(bodyHash, result);
        return result;
      } else {
        // If that response was not OK, throw an error.
        throw Exception('Failed to load data');
      }
    }
  }

  Future<StatsResponse> fetchStats(
      http.Client client, DateTime startDate) async {
    var body = {
      "startDate": startDate.toIso8601String(),
    };

    var bodyHash = body.toString().hashCode;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    final response = await client.post('$baseUrl/api/stats',
        body: body,
        headers: {
          HttpHeaders.authorizationHeader:
              "Bearer ${prefs.getString("accessToken")}"
        });

    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON
      StatsResponse result = StatsResponse.fromJson(json.decode(response.body));
      return result;
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load data');
    }
  }
}
