package gamifybills.gobbackend.repository;

import gamifybills.gobbackend.model.User;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
	Optional<User> findByUsername(String userId);

	List<User> findByIdIn(List<Long> ids);

	Boolean existsByUsername(String userId);
}
