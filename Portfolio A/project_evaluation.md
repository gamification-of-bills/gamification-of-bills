# Project Evaluation

## Our approach

We believed it important to assess and adapt our app continuously to ensure the final product is as effective and useful to the clients as possible. To do this, we used evaluation to help identify areas for improvement and realize our goals more efficiently along the course of the project.

We enabled this by having regular face to face meeting with the clients, as well as phone conferences and online calls, and established a Slack for our project with all of the clients, team members and mentor, providing a place for instant communication. This allowed us to quickly show the clients the app, via an installable APK or just screenshots and videos for feedback. We repeated this process with every major feature or change in the UI.

This constant feedback loop was invaluable in the process of developing the app. By constantly referring to the clients for what they liked/disliked, we could ensure that we were always developing an app that would be valuable to them. We found that the best environment for feedback with the clients was as a face to face meeting, however these were rare due to being based in London, so video calls worked as a good replacement.

A key part of evaluation was as the end of the project neared. We stepped up our evaluation efforts to get a better understanding of how our final product will be percieved by users, as well as what any future steps that could be taken to better our service.

Since the userbase of our app concerns the average person with a smartmeter in their home, we believed it would be best to represent this demographic by asking friends, family, acquaintances and random people that would be kind enough to partake in our survey.

We chose to conduct this as a combination of hands-on interviews and a questionnaire. This was accomplished either as a regular face to face interview or online where people would be given the app if they had the possibility to install it or a brief video demo of it. Afterwards, they would also receive the following questions:

- How would you describe our product in one or more words?
- Were you able to find the information you were looking for on our app?
- How visually appealing is our app?
- How easy to navigate is our app?
- What did you like best about our app?
- What do you find most frustrating about our product?
- If you were to review the app what score would you give it out of 5?
- Would you use this app for your home?
- If you are unhappy with our app what can we do to improve?
- Would you recommend this service to someone else?
- Do you have any other comments, questions, or concerns?

## Feedback

The overall feedback was positive with the majority claiming that they found our app very useful and that they would use it in their own home given the possibility.
 The information was readily available and easy to get to. The most liked feature was the monthly comparison.
However visually there would be room for improvement. Despite that, all users said they would recommend our service to a friend.

This feedback was very helpful, and allowed us to make small adjustments and improvements to our product.

## Sample Feedback

Below there is a transcript of one of the survey answers:

How would you describe our product in one or more words?
"I would say, handy, describes it best."

Were you able to find the information you were looking for on our app?
"Yes it was pretty straight forward, the bottom text could be a bit bigger though."

How visually appealing is our app?
"I like the way it looks, has a nice vibe. The colors look a bit like the ocean to me."

How easy to navigate is our app?
"As I said I feel it s pretty straight forward."

What did you like best about our app?
"The monthly comparison looks really useful. Oh and the measurements, too."

What do you find most frustrating about our product?
"I had some problems tapping on the graphs where I wanted, but that might be just because I have big fingers. "

If you were to review the app what score would you give it out of 5?
"I would say 4 to 5. There could be more things to add."

Would you use this app for your home?
"Yes I would like something like this. It would make my life easier."

If you are unhappy with our app what can we do to improve?
"Well it would be really nice if I could also pay my bills through the app."

Would you recommend this service to someone else?
"Yes, to my neighbours probably. "

Do you have any other comments, questions, or concerns?
"No, not really."


