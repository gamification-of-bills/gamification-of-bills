import 'package:flutter/material.dart';

enum CompareMenu { kwh, pounds, car, kettle }

class PopupItem {
  final String units;

  PopupItem(this.units);
}

typedef UpdateUnitsCallback = void Function(String units);

class ComparePopup extends StatelessWidget {
  final UpdateUnitsCallback updateCallback;

  final Map<CompareMenu, PopupItem> popupItems = {
    CompareMenu.kwh: PopupItem('kWh'),
    CompareMenu.pounds: PopupItem('£/H'),
    CompareMenu.car: PopupItem('Nissan Leaf'),
    CompareMenu.kettle: PopupItem('2 Litre Kettles'),
  };

  ComparePopup(this.updateCallback, {Key key}) : super(key: key);

  Widget build(BuildContext context) {
    return PopupMenuButton<CompareMenu>(
      icon: Icon(Icons.show_chart),
      tooltip: "Graph Units",
      itemBuilder: (BuildContext context) {
        List<PopupMenuEntry<CompareMenu>> gen = [];
        for (CompareMenu item in popupItems.keys) {
          gen.add(PopupMenuItem(
            child: Text(popupItems[item].units),
            value: item,
          ));
        }
        return gen;
      },
      onSelected: (CompareMenu result) {
        updateCallback(popupItems[result].units);
      },
    );
  }
}
