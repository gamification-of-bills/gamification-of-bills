package gamifybills.gobbackend.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import gamifybills.gobbackend.model.EnergyUsage;
import gamifybills.gobbackend.model.EnergyUsageID;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class EnergyUsageSerializer extends JsonSerializer<EnergyUsage> {

	@Override
	public void serialize(EnergyUsage value,
	                      JsonGenerator jgen,
	                      SerializerProvider provider) throws IOException {
		jgen.writeStartObject();
		EnergyUsageID energyUsageId = value.getEnergyUsageID();
//		jgen.writeNumberField("id", energyUsageId.getId());
		jgen.writeStringField("start_time", energyUsageId.getStartTime().toString());
		jgen.writeNumberField("consumption", value.getConsumption());
		jgen.writeEndObject();
	}
}
