package gamifybills.gobbackend.repository;

import gamifybills.gobbackend.model.EnergyUsage;
import gamifybills.gobbackend.model.EnergyUsageID;
import java.sql.Timestamp;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import org.springframework.data.repository.query.Param;

public interface EnergyUsageRepository extends CrudRepository<EnergyUsage, EnergyUsageID> {

	List<EnergyUsage> findByEnergyUsageID_Username(String username);

	List<EnergyUsage> findAllByEnergyUsageID_UsernameAndEnergyUsageID_StartTimeGreaterThanEqualAndEnergyUsageID_StartTimeLessThan(
			String username, Timestamp startTime, Timestamp endTime);

	@Query(
//			value = "select * from energy_usages a where MONTH(a.start_time) = MONTH(:month)",
			value = "select * from energy_usages a where a.start_time >= :month AND a.start_time < DATE_ADD(:month, INTERVAL 1 MONTH)",
			nativeQuery = true)
	@Cacheable("months")
	List<EnergyUsage> findAllWithMonth(
			@Param("month") Timestamp date);

//	List<EnergyUsage> findAllByEnergyUsageID_StartTime_MonthEquals(int month);
}
