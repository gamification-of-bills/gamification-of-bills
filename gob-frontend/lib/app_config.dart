import 'package:flutter/material.dart';

class AppConfig {
  final String appName;
  final ThemeData theme;
  final String apiUrl;

  AppConfig({this.appName, this.theme, this.apiUrl});
}

AppConfig get defaultConfig {
  return AppConfig(
      appName: "Gamification of Bills SPE",
      theme: ThemeData(
          primaryColor: Color(0xFF274B63),
          backgroundColor: Color(0xFF274B63),
          accentColor: Color(0xFF4482AD),
          cardColor: Color(0xFF4380a9),
          canvasColor: Color(0xFF4380a9),
          textTheme: TextTheme(
            title: TextStyle(color: Colors.white),
            subhead: TextStyle(color: Colors.white),
            subtitle: TextStyle(
              color: Colors.white,
            ),
          ),
          iconTheme: IconThemeData(color: Colors.white)),
      apiUrl: "https://spe.ferguslongley.com");
}
