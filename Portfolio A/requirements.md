# Requirements

## Stakeholders

### Baringa Partners

An independent business and technology consultancy (specifically for OVO and Bristol energy) who have consulted us to create the application for the aforementioned energy companies.

### OVO Energy

A Bristol-based energy company interested in reducing energy consumption during peak usage times to lower the costs of supplying electricity.

### Bristol Energy

A Bristol based energy company interested in reducing energy consumption during peak usage times to lower costs of supplying electricity.

### People with smart meters

People who used OVO and Bristol Energy's services that would like to spend less on energy and potentially be more aware of usage.

### Power generation companies

Companies that transfer power from various sources to the grid. It is their best interest to have fewer peaks times and instead process a constant amount of energy per unit time to reduce cost.

### The national grid

To avoid any problems, there is a requirement of reducing stress on the power network at peak hours and keeping it stable.

### Climate change aware entities

People and organizations who would prefer the use of sustainable energy production to help combat climate change. The more stable the grid, the more sustainable sources used.

### System admins

Would like to have an easy to maintain system that causes minimal problems for themselves and users.

## Use-case diagram

![Use case diagram](images/use_case.png)

### Flows of goals

#### Login

##### Basic flow

1. Enter credentials into the app
2. App sends user login to API
3. API verifies user login
4. API sends authorization token for future API calls
5. App confirms login and proceeds to next view

##### Alternative flow

1. User enters incorrect credentials
2. App sends user login to API over HTTPS
3. API verifies user password against hashed user
4. API rejects the login request with 401
5. App recieves error and prompts the user that the login failed, the user can try again.

##### Exceptional flow

1. User logs in but has connection issues, information is lost
2. Server never receives data
3. Login times out in app
4. App tells user to try again due to connection issues

#### See current usage

1. User opens page in app
2. App requests usage data from API with the current month
3. API receives the request with a user auth token and month
4. API checks that the token is valid
5. API retrieves usage data from database concerning the month and user
6. API provides usage data to app
7. App displays the usage data in a graph

#### Manage data

1. OVO/Bristol Energy provide energy consumption data
2. System admin inserts CSV into database using simple script

### Atomic Requirements

The atomic requirements for seeing current usage are as follows:

#### Functional requirements

- USAGE 1.0: The usage data will be of the current month
- USAGE 1.1: The data will be shown as a graph
- USAGE 1.2: The graph will show units
- USAGE 1.3: The user should have at least four options for units on the energy axis. (To allow users to see more relatable measures than KWh)
- AUTH 1.0: The user will need an auth token to make the request (To maintain security of the user data)
- AUTH 1.1: The user will be redirected to login if they have no token (In order to acquire a token)
- INTERFACE 1.0: No graph will be shown if there is no data
- INTERFACE 1.1: An error will be shown if there is no data
- INTERFACE 1.2: The user should be able to return to the previous screen

#### Non-functional requirements

- SECURE 1.0: The system must not give information to a non-logged in user (To maintain security of data)
- SECURE 1.1: The system must only give a user their information and no one else's
- SECURE 2.0: The database that stores user information must only be accessible by the API (To avoid risk of vulnerabilities)
- PERFORM 1.0: The system must respond to a request within 10 seconds
- PERFORM 2.0: The app should load the graph in the less than 10 seconds
- ACCESSIBILITY 1.0: The app must be usable by an untrained user
- ACCESSIBILITY 1.1: Complex features will prompt user with usage explanation.