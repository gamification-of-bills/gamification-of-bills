package gamifybills.gobbackend.payload;

import gamifybills.gobbackend.model.EnergyUsage;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.List;
import java.util.stream.Collectors;

class Pair {
	private final LocalDateTime startDate;
	private final Double consumption;

	Pair(LocalDateTime startDate, Double consumption) {
		this.startDate = startDate;
		this.consumption = consumption;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public Double getConsumption() {
		return consumption;
	}
}

public class IntervalResponse {

	private final LocalDateTime startDate;
	private final LocalDateTime endDate;
	private final List<Pair> dataPoints;

	public IntervalResponse(LocalDateTime startDate, LocalDateTime endDate, List<EnergyUsage> dataPoints, int resolution) {
		this.startDate = startDate;
		this.endDate = endDate;

		// Convert to list of Pairs (Isolating relevant info)
		List<Pair> convertedDataPoints = dataPoints.stream()
				.map((e) -> new Pair(e.getEnergyUsageID().getStartTime().toLocalDateTime(), e.getConsumption())).collect(
						Collectors.toList());

		// Reduce data to required resolution
		if (resolution == 3) {
			convertedDataPoints = convertedDataPoints.stream().map((e) -> new Pair(e.getStartDate().with(
					TemporalAdjusters.firstDayOfMonth()).truncatedTo(ChronoUnit.DAYS), e.getConsumption())).collect(
					Collectors.toList());
		} else {
			ChronoUnit res;
			if (resolution == 1) {
				res = ChronoUnit.HOURS;
			} else if (resolution == 2) {
				res = ChronoUnit.DAYS;
			} else {
				res = ChronoUnit.MINUTES;
			}
			convertedDataPoints = convertedDataPoints.stream().map((e) -> new Pair(e.getStartDate().truncatedTo(res), e.getConsumption())).collect(
					Collectors.toList());
		}

		// Collect data
		this.dataPoints = convertedDataPoints.stream().collect(Collectors.groupingBy(Pair::getStartDate, Collectors.summingDouble(Pair::getConsumption))).entrySet().stream()
				.map((e) -> new Pair(e.getKey(), e.getValue()))
				.collect(Collectors.toList());
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public List<Pair> getDataPoints() {
		return dataPoints;
	}
}
