import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppBarParams {
  final Widget title;
  final List<Widget> actions;
  final Color backgroundColor;

  AppBarParams({this.title, this.actions, this.backgroundColor});
}

class DrawerItem {
  String title;
  String route;
  IconData icon;

  DrawerItem(this.title, this.route, this.icon);
}

class UniversalScaffold extends StatefulWidget {
  final AppBarParams appBarParams;
  final Widget body;
  final String route;

  UniversalScaffold({this.appBarParams, this.body, this.route});

  @override
  UniversalScaffoldState createState() => UniversalScaffoldState();
}

class UniversalScaffoldState extends State<UniversalScaffold> {
  final drawerItems = <DrawerItem>[
    DrawerItem("Dashboard", "/dash", Icons.dashboard),
    DrawerItem("Day view", "/day", Icons.show_chart),
    DrawerItem("Month view", "/month", Icons.show_chart),
    DrawerItem("Year view", "/year", Icons.show_chart)
  ];

  String username;

  Future<String> getUsername() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    username = prefs.getString("username");

    return username;
  }

  Widget _buildDrawer(BuildContext context) {
    var drawerOptions = <Widget>[];
    for (DrawerItem item in drawerItems) {
      drawerOptions.add(ListTile(
        leading: Icon(item.icon),
        title: Text(item.title),
        selected: item.route == widget.route,
        onTap: () {
          Navigator.pop(context);
          Navigator.of(context).pushNamed(item.route);
        },
      ));
    }

    return Theme(
      data: Theme.of(context).copyWith(
        canvasColor: Colors.white
      ),
      child: Drawer(
        child: Column(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: FutureBuilder(
                  future: getUsername(),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done)
                      return Text("User: " + username);
                    else
                      return Container();
                  }),
              accountEmail: null,
            ),
            Column(
              children: drawerOptions,
            ),
            Expanded(
              child: Align(
              alignment: Alignment.bottomLeft,
              child: ListTile(
                leading: Icon(Icons.settings),
                title: Text("Settings"),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.pushNamed(context, '/settings');
                },
              ),
            ))
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter,
              colors: [
                Theme.of(context).backgroundColor.withAlpha(90),
                Theme.of(context).backgroundColor
              ],
              tileMode: TileMode.repeated)),
      child: Scaffold(
        appBar: AppBar(
          title: widget.appBarParams?.title,
          actions: widget.appBarParams?.actions,
          elevation: 0,
          backgroundColor: Color(0x00000000),
        ),
        backgroundColor: Colors.transparent,
        drawer: _buildDrawer(context),
        body: widget.body,
      ),
    );
  }
}
