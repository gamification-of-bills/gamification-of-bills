import csv
import random

import bcrypt

NUM_IDS = 1000

with open("passwords.txt") as pass_file:
    passwords = pass_file.read().splitlines()

login_file = open('actual_login.csv', mode='w')
login_writer = csv.writer(login_file)

login_db_file = open('actual_login_db.csv', mode='w')
login_db_writer = csv.writer(login_db_file)

for ident in range(1, NUM_IDS+1):
    pw = random.choice(passwords)
    salt = bcrypt.gensalt(rounds=10, prefix=b"2a")
    hashed = bcrypt.hashpw(pw.encode(), salt)

    login_writer.writerow([ident, pw])
    login_db_writer.writerow([ident, hashed.decode()])

login_file.close()
login_db_file.close()
