package gamifybills.gobbackend.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.sql.Timestamp;

@Embeddable
public class EnergyUsageID implements Serializable {
	@Column(name = "username", nullable = false, length = 60)
	private String username;

	@Column(name = "start_time", nullable = false)
	private Timestamp startTime;

	public EnergyUsageID() {
	}

	public EnergyUsageID(String username, Timestamp startTime) {
		this.username = username;
		this.startTime = startTime;
	}

	public String getUsername() {
		return username;
	}

	public Timestamp getStartTime() {
		return startTime;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
}
