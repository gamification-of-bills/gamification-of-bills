import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  final DecorationImage image;
  Logo({this.image});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 128.0,
      height: 128.0,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        image: image,
      ),
    );
  }
}
