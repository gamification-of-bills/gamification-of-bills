import 'package:frontend/Models/Usage.dart';

class IntervalResponse {
  final int id;
  final DateTime startDate;
  final DateTime endDate;
  final List<Usage> dataPoints;

  IntervalResponse({this.id, this.startDate, this.endDate, this.dataPoints});

  factory IntervalResponse.fromJson(Map<String, dynamic> json) {
    List datas = json['dataPoints'];
    List<Usage> dataPoints =
        datas.map((usage) => Usage.fromJson(usage)).toList();
    return IntervalResponse(
      id: json['id'],
      startDate: DateTime.parse(json['startDate']),
      endDate: DateTime.parse(json['endDate']),
      dataPoints: dataPoints,
    );
  }
}
